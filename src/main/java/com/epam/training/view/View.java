package com.epam.training.view;

import com.epam.training.controller.task3.FibonacciExeImpl;
import com.epam.training.controller.task5.SleepingThreadImpl;
import com.epam.training.model.task1.PingPong;
import com.epam.training.model.task2.Fibonacci;
import com.epam.training.model.task4.FibonacciWithCallable;
import com.epam.training.model.task6.ThreadSyncTesting;
import com.epam.training.model.task7.PipedThreads;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class View {
    private static final Logger logger = LogManager.getLogger(View.class.getName());
    private Map<String, String> menu;
    private Map<String, Runnable> methodsMenu;
    private Scanner scanner;
    private Locale locale;
    private ResourceBundle bundle;

    public View() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
    }

    public void run(){
        String keyMenu;
        do {
            generateMainMenu();
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                while (keyMenu.equals("")){
                    keyMenu = scanner.nextLine().toUpperCase();
                }
                methodsMenu.get(keyMenu).run();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void generateMainMenu() {
        menu.clear();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("7", bundle.getString("7"));
        menu.put("8", bundle.getString("8"));
        menu.put("9", bundle.getString("9"));
        menu.put("Q", bundle.getString("Q"));

        methodsMenu.clear();
        methodsMenu.put("1", this::i18nEnglish);
        methodsMenu.put("2", this::i18nUkrainian);
        methodsMenu.put("3", this::startPingPong);
        methodsMenu.put("4", this::startFibonacci);
        methodsMenu.put("5", this::startFibonacciExecutors);
        methodsMenu.put("6", this::startFibonacciCallable);
        methodsMenu.put("7", this::startSleepingThread);
        methodsMenu.put("8", this::startSyncTesting);
        methodsMenu.put("9", this::startPipes);
        outputMenu();
    }

    private void i18nEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu");
    }

    private void i18nUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
    }

    private void startPingPong(){
        PingPong.run();
    }

    private void startFibonacci(){
        Fibonacci.run();
    }

    private void startFibonacciExecutors(){
        new FibonacciExeImpl().run();
    }

    private void startFibonacciCallable(){
        FibonacciWithCallable.run();
    }

    private void startSleepingThread(){
        logger.trace("Please enter number of tasks");
        new SleepingThreadImpl().scheduledTaskRun(scanner.nextInt());
    }

    private void startSyncTesting(){
        new ThreadSyncTesting().runThreeThreads();
    }

    private void startPipes(){
        new PipedThreads().runThreads();
    }

    private void outputMenu() {
        logger.trace("MENU:");
        for (String option : menu.values()) {
            logger.trace(option);
        }
        logger.trace(">>> ");
    }

}
