package com.epam.training.model.task2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.ResourceBundle;

public class Fibonacci {
    private static final Logger logger = LogManager.getLogger(Fibonacci.class.getName());
    private static long[] fibonacciArr;
    private static int pointer;
    private static final Object obj = new Object();

    private static class FibonacciThread extends Thread {
        public void run() {
            long next;
            while (pointer < fibonacciArr.length) {
                next = nextNumber();
                logger.info("next- " + next + ", " + getName());

            }
            logger.info("Thread finished work " + getName());
        }
    }

    private Fibonacci() {
        fibonacciArr =
                new long[Integer.parseInt(
                        ResourceBundle.getBundle("values")
                                .getString("FibonacciLength"))];
        fibonacciArr[0] = 0;
        fibonacciArr[1] = 1;
        pointer = 2;
    }

    public static void run() {
        new Fibonacci();
        FibonacciThread firstThread = new FibonacciThread();
        FibonacciThread secondThread = new FibonacciThread();
        FibonacciThread thirdThread = new FibonacciThread();
        firstThread.start();
        secondThread.start();
        thirdThread.start();
        try {
            firstThread.join();
            secondThread.join();
            thirdThread.join();
        } catch (InterruptedException ignored) {
        }
        logger.info(Arrays.toString(fibonacciArr));
    }

    private static long nextNumber() {
        synchronized (obj) {
            long newValue = fibonacciArr[pointer - 1] + fibonacciArr[pointer - 2];
            fibonacciArr[pointer] = newValue;
            pointer++;
            return newValue;
        }
    }
}
