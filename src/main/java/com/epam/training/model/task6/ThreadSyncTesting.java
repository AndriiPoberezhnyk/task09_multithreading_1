package com.epam.training.model.task6;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalTime;

public class ThreadSyncTesting {
    private final Logger logger = LogManager.getLogger(ThreadSyncTesting.class.getName());
    private static int incrementBy10 = 0;
    private static int decrementByMinus10 = 0;
    private static int sqr = 2;
    private static final Object obj1 = new Object();
    private static final Object obj2 = new Object();
    private static final Object obj3 = new Object();

    public ThreadSyncTesting() {

    }

    public class FirstThread implements Runnable {
        @Override
        public void run() {
            methodInc();
        }
    }

    public class SecondThread implements Runnable {
        @Override
        public void run() {
            methodDec();
        }
    }

    public class ThirdThread implements Runnable {
        @Override
        public void run() {
            methodSqr();
        }
    }

    public void runThreeThreads() {
        Thread first = new Thread(new FirstThread());
        Thread second = new Thread(new SecondThread());
        Thread third = new Thread(new ThirdThread());
        first.start();
        second.start();
        third.start();
        try {
            first.join();
            second.join();
            third.join();
        } catch (InterruptedException e) {
            logger.warn("Interrupt third thread");
        }

    }

    private void methodInc() {
        synchronized (obj1) {
            logger.info(Thread.currentThread().getName() + " started at " + LocalTime.now());
            for (int i = 0; i < 10; i++) {
                incrementBy10++;
            }
            logger.info(Thread.currentThread().getName() + " incrementing");
            logger.info(Thread.currentThread().getName() + " -> " + incrementBy10
                    + " |" + LocalTime.now());
        }
    }

    private void methodDec() {
        synchronized (obj2) {
            logger.info(Thread.currentThread().getName() + " started at " + LocalTime.now());
            for (int i = 0; i < 10; i++) {
                decrementByMinus10--;
            }
            logger.info(Thread.currentThread().getName() + " decrementing");
            logger.info(Thread.currentThread().getName() + " -> " + decrementByMinus10
                    + " |" + LocalTime.now());
        }
    }

    private void methodSqr() {
        synchronized (obj3) {
            logger.info(Thread.currentThread().getName() + " started at " + LocalTime.now());
            sqr *= sqr;
            logger.info(Thread.currentThread().getName() + " sqr");
            logger.info(Thread.currentThread().getName() + " -> " + sqr + " |" + LocalTime.now());
        }
    }

}
