package com.epam.training.model.task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class PingPong {
    private static final Logger logger = LogManager.getLogger(PingPong.class.getName());

    public static void run() {
        Scanner scanner = new Scanner(System.in);
        Thread ping = new Thread(
                () -> {
                    try {
                        while (true) {
                            logger.info("ping");
                            Thread.sleep(1000);
                        }
                    } catch (InterruptedException ignored) {
                    }
                });
        Thread pong = new Thread(
                () -> {
                    try {
                        Thread.sleep(500);
                        while (true) {
                            logger.info("pong");
                            Thread.sleep(1000);
                        }
                    } catch (InterruptedException ignored) {
                    }
                });
        ping.start();
        pong.start();
        String event = scanner.nextLine();
        if (event != null) {
            ping.interrupt();
            pong.interrupt();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
