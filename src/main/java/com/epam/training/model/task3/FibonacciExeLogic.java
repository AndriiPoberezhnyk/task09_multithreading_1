package com.epam.training.model.task3;

import java.util.ResourceBundle;
import java.util.concurrent.*;

public class FibonacciExeLogic implements FibonacciExeModel {
    private FibonacciExe fibonacciExe;
    private FibonacciExe.FibonacciRunnable fibonacciRunnable;

    public FibonacciExeLogic() {
        fibonacciExe = new FibonacciExe();
        fibonacciRunnable = fibonacciExe.new FibonacciRunnable();
    }

    @Override
    public void runExecutor() {
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(fibonacciRunnable);
    }

    @Override
    public void runExecutorService() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(fibonacciRunnable);
        executor.shutdown();
    }

    @Override
    public void runScheduledExecutorService() {
        int repeats = Integer.parseInt(ResourceBundle.getBundle("values")
                .getString("ScheduledExecutorRepeats"));
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate(fibonacciRunnable, 1, 2, TimeUnit.SECONDS);
        try {
            Thread.sleep(2000);
            int count = 0;
            while (!executor.isShutdown()) {
                initNewArray();
                Thread.sleep(2000);
                count++;
                if (repeats == count) {
                    executor.shutdown();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initNewArray() {
        fibonacciExe.initArray();
    }
}
