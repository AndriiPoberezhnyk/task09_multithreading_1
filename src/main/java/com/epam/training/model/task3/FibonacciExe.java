package com.epam.training.model.task3;

import com.epam.training.model.task2.Fibonacci;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ResourceBundle;

class FibonacciExe {
    private final Logger logger = LogManager.getLogger(Fibonacci.class.getName());
    private long[] fibonacciArr;
    private int pointer;

    class FibonacciRunnable implements Runnable {
        @Override
        public void run() {
            Thread thread = Thread.currentThread();
            long next;
            while (pointer < fibonacciArr.length) {
                next = nextNumber();
                logger.info("next- " + next + ", " + thread.getName());
            }
            logger.info("Thread finished work " + thread.getName());
        }
    }

    FibonacciExe() {
        initArray();
    }

    private long nextNumber() {
        long newValue = fibonacciArr[pointer - 1] + fibonacciArr[pointer - 2];
        fibonacciArr[pointer] = newValue;
        pointer++;
        return newValue;
    }

    void initArray() {
        fibonacciArr =
                new long[Integer.parseInt(
                        ResourceBundle.getBundle("values")
                                .getString("FibonacciLength"))];
        fibonacciArr[0] = 0;
        fibonacciArr[1] = 1;
        pointer = 2;
    }
}
