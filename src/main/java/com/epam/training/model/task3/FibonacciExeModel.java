package com.epam.training.model.task3;

public interface FibonacciExeModel {

    void runExecutor();

    void runExecutorService();

    void runScheduledExecutorService();

    void initNewArray();
}
