package com.epam.training.model.task7;

import java.io.IOException;
import java.io.PipedInputStream;
import java.time.LocalTime;
import java.util.ResourceBundle;

public class Consumer implements Runnable {

    private final PipedInputStream pipedInputStream;

    Consumer(PipedInputStream pipedInputStream) {
        this.pipedInputStream = pipedInputStream;
    }

    @Override
    public void run() {
        int limit = Integer.parseInt(ResourceBundle.getBundle("values")
                .getString("MaxGeneratedValue"));
        try {
            while (true) {
                int value = pipedInputStream.read();
                System.out.println("Consumer thread consuming: " + value
                        + " " + LocalTime.now());
                Thread.sleep(50);
                if (value == limit)
                    break;
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                pipedInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
