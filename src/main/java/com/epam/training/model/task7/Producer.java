package com.epam.training.model.task7;

import java.io.IOException;
import java.io.PipedOutputStream;
import java.time.LocalTime;
import java.util.ResourceBundle;

public class Producer implements Runnable {

    private final PipedOutputStream pipedOutputStream;

    Producer(PipedOutputStream pipedOutputStream) {
        this.pipedOutputStream = pipedOutputStream;
    }

    @Override
    public void run() {
        int index = 0;
        int limit = Integer.parseInt(ResourceBundle.getBundle("values")
                .getString("MaxGeneratedValue"));
        try {
            while (index <= limit) {
                System.out.println("Producer thread generating: " + index +
                        " " + LocalTime.now());
                pipedOutputStream.write(index);
                Thread.sleep(50);
                index++;
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                pipedOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
