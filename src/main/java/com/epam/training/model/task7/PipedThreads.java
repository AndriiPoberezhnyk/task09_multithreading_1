package com.epam.training.model.task7;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class PipedThreads {

    public void runThreads() {
        PipedOutputStream pipedOutputStream = new PipedOutputStream();
        PipedInputStream pipedInputStream = null;
        try {
            pipedInputStream = new PipedInputStream(pipedOutputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Thread prodThread = new Thread(new Producer(pipedOutputStream));
        Thread consThread = new Thread(new Consumer(pipedInputStream));

        prodThread.start();
        consThread.start();
        try {
            prodThread.join();
            consThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                pipedOutputStream.close();
                pipedInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
