package com.epam.training.model.task5;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.util.ResourceBundle;

public class SleepingThread extends Thread {
    private static final Logger logger = LogManager.getLogger(SleepingThread.class.getName());

    public void run() {
        int bounds = Integer.parseInt(ResourceBundle.getBundle("values")
                .getString("SleepingTime"));
        int sleepFor = (1 + new Random().nextInt(bounds)) * 1000;
        try {
            Thread.sleep(sleepFor);
        } catch (InterruptedException ignored) {
            logger.warn("Thread sleep for" + sleepFor / 1000 + " seconds " +
                    "interrupted");
        }
        logger.info(LocalTime.now() + " Thread " + Thread.currentThread().getName()
                + " slept for " + sleepFor / 1000 + " seconds");
    }
}
