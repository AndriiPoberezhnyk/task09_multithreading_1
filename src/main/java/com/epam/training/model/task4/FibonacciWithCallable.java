package com.epam.training.model.task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ResourceBundle;
import java.util.concurrent.*;

public class FibonacciWithCallable {
    private static final Logger logger = LogManager.getLogger(FibonacciWithCallable.class.getName());
    private static int length;

    private static class FibonacciThread implements Callable<Long> {
        @Override
        public Long call() throws Exception {
            long[] fibonacciArr = initArray();
            int pointer = Integer.parseInt(
                    ResourceBundle.getBundle("values")
                            .getString("FibonacciStartIndex"));
            long sum = 1;
            long next;
            while (pointer < length) {
                next = nextNumber(fibonacciArr, pointer);
                pointer++;
                sum += next;
                logger.info("next- " + next + ", " + Thread.currentThread().getName());
            }
            return sum;
        }
    }

    private FibonacciWithCallable() {
        length = Integer.parseInt(
                ResourceBundle.getBundle("values")
                        .getString("FibonacciLength"));
    }

    public static void run() {
        new FibonacciWithCallable();
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        Future<Long> futureFirst =
                executorService.submit(new FibonacciThread());
        Future<Long> futureSecond =
                executorService.submit(new FibonacciThread());
        Future<Long> futureThird =
                executorService.submit(new FibonacciThread());
        try {
            logger.info("First thread sum = " + futureFirst.get());
            logger.info("Second thread sum = " + futureSecond.get());
            logger.info("Third thread sum = " + futureThird.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        executorService.shutdown();
    }

    private static long nextNumber(long[] fibonacciArr, int pointer) {
        long newValue = fibonacciArr[pointer - 1] + fibonacciArr[pointer - 2];
        fibonacciArr[pointer] = newValue;
        return newValue;
    }

    private static long[] initArray() {
        long[] fibonacciArr = new long[length];
        fibonacciArr[0] = 0;
        fibonacciArr[1] = 1;
        return fibonacciArr;
    }
}
