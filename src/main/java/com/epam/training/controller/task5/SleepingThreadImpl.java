package com.epam.training.controller.task5;

import com.epam.training.model.task5.SleepingThread;

import java.util.Scanner;
import java.util.concurrent.*;

public class SleepingThreadImpl implements SleepingThreadController {

    public SleepingThreadImpl() {
    }

    @Override
    public void scheduledTaskRun(int repeats) {
        ScheduledExecutorService executor =
                Executors.newScheduledThreadPool(repeats);
        executor.scheduleAtFixedRate(new SleepingThread(), 0, 1,
                TimeUnit.SECONDS);
        String event = new Scanner(System.in).nextLine();
        if (event != null) {
            executor.shutdownNow();
            try {
                Thread.sleep(100);
            } catch (InterruptedException ignored) {
            }
        }
    }
}
