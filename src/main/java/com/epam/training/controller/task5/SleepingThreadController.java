package com.epam.training.controller.task5;

public interface SleepingThreadController {

    void scheduledTaskRun(int repeats);
}
