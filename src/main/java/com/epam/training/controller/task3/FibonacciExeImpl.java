package com.epam.training.controller.task3;

import com.epam.training.model.task2.Fibonacci;

import com.epam.training.model.task3.FibonacciExeLogic;
import com.epam.training.model.task3.FibonacciExeModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FibonacciExeImpl implements FibonacciExeController {
    private final Logger logger = LogManager.getLogger(Fibonacci.class.getName());
    private FibonacciExeModel fibonacciExe;

    public FibonacciExeImpl() {
        fibonacciExe = new FibonacciExeLogic();
    }

    @Override
    public void run() {
        try {
            logger.info("Executor -----------------------------------------");
            fibonacciExe.runExecutor();
            Thread.sleep(100);
            fibonacciExe.initNewArray();
            logger.info("ExecutorService ----------------------------------");
            fibonacciExe.runExecutorService();
            Thread.sleep(100);
            fibonacciExe.initNewArray();
            logger.info("ScheduledExecutorService -------------------------");
            fibonacciExe.runScheduledExecutorService();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
